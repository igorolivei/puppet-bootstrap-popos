$packages = [
  'vim',
  'tmux',
  'python3-dev',
  'python3-pip',
  'ansible',
]

$packages_pc = [
  'meld',
  'spotify-client',
]

$packages_snap = [
  'helm',
  'kubectl',
  'go',
  'terragrunt',
  'docker',
  'atom',
  'skype',
  'drawio',
]

package { $packages:
  ensure => latest,
}

class { 'snapd': }

package { $packages_snap:
  ensure   => latest,
  provider => snap,
  require  => Class['snapd'],
}

# validate if it's computer
package { $packages_pc:
  ensure => latest,
}

# # k3s
class { 'k3s':
  installation_mode => 'binary',
}

# # terraform
class { 'hashicorp_install':
  packages => {
    'terraform' => '0.15.5',
    'vagrant'   => '2.2.16', # validate if it's computer
  }
}

# # docker
class { 'docker::compose':
  ensure  => present,
  version => '1.9.0',
}

# # aws cli
class { 'awscli': } # ensure version? pass credentials?

# #### compiuter
# # chrome
class { 'google_chrome': }

# # virtualbox
class { 'virtualbox': }

Service {
  provider => 'systemd',
}
